define(['jquery'], function ($) {
    'use strict';
    return function () {
        $.validator.addMethod(
            'validate-linkedin-profile',
            function (value, element) {
                return /^(http(s)?:\/\/)?([\w]+\.)?linkedin\.com\/(pub|in|profile)/gm.test(value) &&
                    /^.{1,250}$/.test(value);
            },
            $.mage.__('Please enter a valid Linkedin Profile')
        )
    }
});
